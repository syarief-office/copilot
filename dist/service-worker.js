/**
 * Copyright 2015 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren */
'use strict';


importScripts("scripts/sw/sw-toolbox.js","scripts/sw/runtime-caching.js");


/* eslint-disable quotes, comma-spacing */
var PrecacheConfig = [["dist/basic.html","895b75091d0180b5f37482b94d2f5c1e"],["dist/images/agency-analytic-icon.png","a9e69aede13f2b267358e1affeba565e"],["dist/images/agency-analytic-icon@2x.png","bc2d1fb6cea4968a22fdb3c5397914dc"],["dist/images/authority-labs-icon.png","8b93daf12234b2437104fca585b34b0c"],["dist/images/authority-labs-icon@2x.png","46dfec9aec1b51059806974aa1960796"],["dist/images/commentar.jpg","f28c8262d25661f34adb574c13e84453"],["dist/images/copilot-about-border-bottom.png","a3f5986024b6a16950b237ab18caca95"],["dist/images/copilot-about-masthead.png","165c07351b12b3d91b75129aa18fd827"],["dist/images/copilot-bg-logres.png","b80adc9fbc84af8f96eb3983a808dbc9"],["dist/images/copilot-blog-masthead.png","20c3802c3a093adaa0d88a376d8a5ef4"],["dist/images/copilot-chart.png","44c3fe1e11e7b599fa65a9579e9f199b"],["dist/images/copilot-dashboard.png","b97270601262464fd2d139292e217f72"],["dist/images/copilot-icon-deep-analytic.png","e29cc6b7436547c073f1e967d1014fb8"],["dist/images/copilot-icon-deep-analytic@2x.png","5f6f1113da9fa1237cb96347386b9189"],["dist/images/copilot-icon-default-blog-01.png","465982bdc391fe07d3b34881bfc92c1e"],["dist/images/copilot-icon-default-blog-01@2x.png","d8b5c0a7570fc62e52e980385749c2e0"],["dist/images/copilot-icon-default-blog-02.png","08eae8ca33d0208810b62f8151585ad5"],["dist/images/copilot-icon-default-blog-02@2x.png","c773b1a2cf577d17820a1e7537fd3bdd"],["dist/images/copilot-icon-default-blog-03.png","56834bb00943724760c13080b0e5dcec"],["dist/images/copilot-icon-default-blog-03@2x.png","92f1b13b2bac0429804bf6832de7e611"],["dist/images/copilot-icon-default-blog-04.png","ec961bedbe8ac970d76c959d617de2fc"],["dist/images/copilot-icon-default-blog-04@2x.png","e19c76e1dff4080a3c9a5762bc156bfb"],["dist/images/copilot-icon-default-blog-05.png","05ff3b81e099c844e8b6a770cb9f003f"],["dist/images/copilot-icon-default-blog-05@2x.png","244d11821c874ca593c1970ae51762ae"],["dist/images/copilot-icon-default-blog-06.png","95e294ce41eceec70d05aa35702ccbcc"],["dist/images/copilot-icon-default-blog-06@2x.png","4cf36c32fa32e4bb3ab882ae6aa09852"],["dist/images/copilot-icon-default-blog-07.png","c4253f7f8e54d75d426315e616057449"],["dist/images/copilot-icon-default-blog-07@2x.png","385268c78c32b71cb64cf020909ba1fe"],["dist/images/copilot-icon-default-blog-08.png","8c875b879f11e64c569c791e254518c0"],["dist/images/copilot-icon-default-blog-08@2x.png","47d2e1668ef0d596b3c1d83f4a85f20c"],["dist/images/copilot-icon-default-blog-09.png","ce687c9fa88b784235a15d0ae5a02fc2"],["dist/images/copilot-icon-default-blog-09@2x.png","5ace179105f78bf9fe16824f6eaa3a98"],["dist/images/copilot-icon-easy-reading-chart.png","649f0ba71af6cc9add05ca75d8b5fdf1"],["dist/images/copilot-icon-easy-reading-chart@2x.png","36d5075f54acc0061a31c5c44cb5bf9c"],["dist/images/copilot-icon-email-intelligence-system.png","2d7f54b0f1c57d2e8bd8c7556ad90b31"],["dist/images/copilot-icon-email-intelligence-system@2x.png","e0f2c04813d4d6981bc130e54d8350bb"],["dist/images/copilot-icon-user-experience.png","5cb7181477753258f5bbe9daffa36877"],["dist/images/copilot-icon-user-experience@2x.png","0b5a860db1bfad434581883360033171"],["dist/images/copilot-icon-white.png","ff7bcbf42d015c3fd63723d0d7164851"],["dist/images/copilot-icon.png","25b709fcdaa9abb7ca989fd8587e3afe"],["dist/images/copilot-icon@2x.png","b8b28fab90f9490d43841a99cb382da3"],["dist/images/copilot-logo.png","88fc7cef261b775f1e314b4736d7e664"],["dist/images/copilot-logo@2x.png","bcb807aca64cba0b4ea08d864e713f93"],["dist/images/copilot-map-contact.png","8b9b0f494b1a7ddda5966b3d7d751bcc"],["dist/images/copilot-pattern-01.png","d5e760d4f4be0dcf8ddc297db48fcf98"],["dist/images/copilot-pattern-02.png","3a369f8530460bcef0ed7f80a80f8bf1"],["dist/images/copilot-pattern.png","19d5f196edb36ed1ad9ddf037d94c91c"],["dist/images/copilot-press-masthead.png","407a39a157880ca9ec2e7ca28c987f2d"],["dist/images/copilot-pricing-masthead.png","56504f6e995ee1a80aa2e8a40507fa61"],["dist/images/copilot-team-01.png","6686f36915ae0d4419999567014d7c74"],["dist/images/copilot-team-02.png","9ce59ab7f8550de0436d5bbbf7e40837"],["dist/images/copilot-team-03.png","f7c9e0931fa545475acc3833430a34ab"],["dist/images/copilot-team-04.png","d6a33f484f23898919626d66f74f7d17"],["dist/images/copilot-team-05.png","bf0e8d8e0a2333f9489cfa08840a2840"],["dist/images/copilot-team-06.png","f5c9be117098a5bbe47606fc35918f87"],["dist/images/google-analytic-icon.png","a22bf1de0e2c9400ab4f21fdabede506"],["dist/images/google-analytic-icon@2x.png","0e77dce536df69c08100a3c3f71aafe3"],["dist/images/hamburger.svg","d2cb0dda3e8313b990e8dcf5e25d2d0f"],["dist/images/majestic-icon.png","c45fb2d676c2eac410ab050d242c30cb"],["dist/images/majestic-icon@2x.png","219d80e5bd29f3fe3481dea184e959da"],["dist/images/pingdom-icon.png","8ca74e4b9269ae1c38b7923717c210ab"],["dist/images/pingdom-icon@2x.png","5cb78af023070affb7ec5b38c08ce34d"],["dist/images/semrush-icon.png","c8865ad4fc79a818c822a9a706073e02"],["dist/images/semrush-icon@2x.png","0db81d0ea65a83536a382af22985dd3e"],["dist/images/touch/apple-touch-icon.png","26f698abeee0d68111889be68ef71995"],["dist/images/touch/chrome-touch-icon-192x192.png","7bb2e564f899a3cb058d2374cb5d2217"],["dist/images/touch/icon-128x128.png","597722f27f95ea307b7e57653361a79d"],["dist/images/touch/icon.png","15498980f98c2bf37ae9897ec4e4ced4"],["dist/images/touch/ms-touch-icon-144x144-precomposed.png","4a3d53c50efc9815088ec4b601d7881a"],["dist/images/user-01.png","47b1290eb75b5eceb73f50cbb55b1c99"],["dist/images/user-02.png","f0ee298fdb98936109f6c7728117d087"],["dist/images/user-03.png","be568eb5176382da586ae3ae3f0a7828"],["dist/images/user-04.png","ae5d0059e82d39afae9104c37430a902"],["dist/images/user-05.png","5f309e004a4d82d7de9887e979a072e9"],["dist/images/user-06.png","200f2b82c778b990f4b63f5b0aef126d"],["dist/images/user-07.png","176a4862cd716c7f80714164f3a22154"],["dist/images/user-08.png","e6263ae9f6644f68e54463e6402e78ad"],["dist/images/user-09.png","1b1ab1c30e25b418b31627d1ff366fe7"],["dist/images/user-10.png","c0d3dc187e68548c890e2965d2a8ce81"],["dist/index.html","4859f7d0b847ca56ded8b2d4fd1077bf"],["dist/manifest.json","b6c29e3e957510f66e350f514334cea6"],["dist/page-about.html","c8cede2a09032c242b3b70b5b4c2eaa4"],["dist/page-blog-detail.html","67754731e321ca345746fa71fc75d6f7"],["dist/page-blog.html","38994d214a803530e3638f424353f576"],["dist/page-help-detail.html","6418e888692f5d93d4c3df03b875459f"],["dist/page-help.html","60eb7df18a427a16c2f226fc06d4f386"],["dist/page-home.html","f0a6eacb02dbed7b9e5969aecc147b4b"],["dist/page-login.html","91266f0a0bed1be7cebce3113ecd870d"],["dist/page-press.html","d43fedf048c233e5f9d4431d3935ea71"],["dist/page-pricing-two.html","b871d5154f8687a157eee9bd9b29b7cb"],["dist/page-pricing.html","bd2d8cbb88056f1e69b302199403496e"],["dist/page-register.html","0dc69757cbd41c7e95a4bad359f8093e"],["dist/scripts/main.js","d9074a6004c41a0ddacde7d0596ebc74"],["dist/scripts/sw/runtime-caching.js","e3e34dcb62b5d62453b9215961585488"],["dist/scripts/sw/sw-toolbox.js","42dd9073ba0a0c8e0ae2230432870678"],["dist/styles/main.css","12936f0dcc39ca64c9698f22285bf42a"],["dist/styles/plugins.css","6a5d1253cb2b1dcb3891bd742db8da33"],["dist/styles/preview.css","ffb286390e18ea845f0609df7fa78fb6"]];
/* eslint-enable quotes, comma-spacing */
var CacheNamePrefix = 'sw-precache-v1-web-starter-kit-' + (self.registration ? self.registration.scope : '') + '-';


var IgnoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function (originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var getCacheBustedUrl = function (url, now) {
    now = now || Date.now();

    var urlWithCacheBusting = new URL(url);
    urlWithCacheBusting.search += (urlWithCacheBusting.search ? '&' : '') + 'sw-precache=' + now;

    return urlWithCacheBusting.toString();
  };

var populateCurrentCacheNames = function (precacheConfig,
    cacheNamePrefix, baseUrl) {
    var absoluteUrlToCacheName = {};
    var currentCacheNamesToAbsoluteUrl = {};

    precacheConfig.forEach(function(cacheOption) {
      var absoluteUrl = new URL(cacheOption[0], baseUrl).toString();
      var cacheName = cacheNamePrefix + absoluteUrl + '-' + cacheOption[1];
      currentCacheNamesToAbsoluteUrl[cacheName] = absoluteUrl;
      absoluteUrlToCacheName[absoluteUrl] = cacheName;
    });

    return {
      absoluteUrlToCacheName: absoluteUrlToCacheName,
      currentCacheNamesToAbsoluteUrl: currentCacheNamesToAbsoluteUrl
    };
  };

var stripIgnoredUrlParameters = function (originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var mappings = populateCurrentCacheNames(PrecacheConfig, CacheNamePrefix, self.location);
var AbsoluteUrlToCacheName = mappings.absoluteUrlToCacheName;
var CurrentCacheNamesToAbsoluteUrl = mappings.currentCacheNamesToAbsoluteUrl;

function deleteAllCaches() {
  return caches.keys().then(function(cacheNames) {
    return Promise.all(
      cacheNames.map(function(cacheName) {
        return caches.delete(cacheName);
      })
    );
  });
}

self.addEventListener('install', function(event) {
  var now = Date.now();

  event.waitUntil(
    caches.keys().then(function(allCacheNames) {
      return Promise.all(
        Object.keys(CurrentCacheNamesToAbsoluteUrl).filter(function(cacheName) {
          return allCacheNames.indexOf(cacheName) === -1;
        }).map(function(cacheName) {
          var urlWithCacheBusting = getCacheBustedUrl(CurrentCacheNamesToAbsoluteUrl[cacheName],
            now);

          return caches.open(cacheName).then(function(cache) {
            var request = new Request(urlWithCacheBusting, {credentials: 'same-origin'});
            return fetch(request).then(function(response) {
              if (response.ok) {
                return cache.put(CurrentCacheNamesToAbsoluteUrl[cacheName], response);
              }

              console.error('Request for %s returned a response with status %d, so not attempting to cache it.',
                urlWithCacheBusting, response.status);
              // Get rid of the empty cache if we can't add a successful response to it.
              return caches.delete(cacheName);
            });
          });
        })
      ).then(function() {
        return Promise.all(
          allCacheNames.filter(function(cacheName) {
            return cacheName.indexOf(CacheNamePrefix) === 0 &&
                   !(cacheName in CurrentCacheNamesToAbsoluteUrl);
          }).map(function(cacheName) {
            return caches.delete(cacheName);
          })
        );
      });
    }).then(function() {
      if (typeof self.skipWaiting === 'function') {
        // Force the SW to transition from installing -> active state
        self.skipWaiting();
      }
    })
  );
});

if (self.clients && (typeof self.clients.claim === 'function')) {
  self.addEventListener('activate', function(event) {
    event.waitUntil(self.clients.claim());
  });
}

self.addEventListener('message', function(event) {
  if (event.data.command === 'delete_all') {
    console.log('About to delete all caches...');
    deleteAllCaches().then(function() {
      console.log('Caches deleted.');
      event.ports[0].postMessage({
        error: null
      });
    }).catch(function(error) {
      console.log('Caches not deleted:', error);
      event.ports[0].postMessage({
        error: error
      });
    });
  }
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    var urlWithoutIgnoredParameters = stripIgnoredUrlParameters(event.request.url,
      IgnoreUrlParametersMatching);

    var cacheName = AbsoluteUrlToCacheName[urlWithoutIgnoredParameters];
    var directoryIndex = 'index.html';
    if (!cacheName && directoryIndex) {
      urlWithoutIgnoredParameters = addDirectoryIndex(urlWithoutIgnoredParameters, directoryIndex);
      cacheName = AbsoluteUrlToCacheName[urlWithoutIgnoredParameters];
    }

    var navigateFallback = '';
    // Ideally, this would check for event.request.mode === 'navigate', but that is not widely
    // supported yet:
    // https://code.google.com/p/chromium/issues/detail?id=540967
    // https://bugzilla.mozilla.org/show_bug.cgi?id=1209081
    if (!cacheName && navigateFallback && event.request.headers.has('accept') &&
        event.request.headers.get('accept').includes('text/html')) {
      var navigateFallbackUrl = new URL(navigateFallback, self.location);
      cacheName = AbsoluteUrlToCacheName[navigateFallbackUrl.toString()];
    }

    if (cacheName) {
      event.respondWith(
        // Rely on the fact that each cache we manage should only have one entry, and return that.
        caches.open(cacheName).then(function(cache) {
          return cache.keys().then(function(keys) {
            return cache.match(keys[0]).then(function(response) {
              if (response) {
                return response;
              }
              // If for some reason the response was deleted from the cache,
              // raise and exception and fall back to the fetch() triggered in the catch().
              throw Error('The cache ' + cacheName + ' is empty.');
            });
          });
        }).catch(function(e) {
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});

