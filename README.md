## Copilot
Copilot is inspired by google starter kit https://github.com/google/web-starter-kit

## Feature
- `Web-starter-kit`
- `Gulp`
- `Bower`
- `Sass Compass`
- `Npm`
- `SMACSS`
- `Browsersync`
- `Pagespeed insights performance`
